import express from 'express'

 const routeApi:express.Router=express.Router();

routeApi.get('/',(req:express.Request,res:express.Response)=>{
    res.status(200).send(`<h1>Welcome to my Api</h1>`)
})

routeApi.get('/posts',(req:express.Request,res:express.Response)=>{
    res.status(200).send(`<h1> testing api</h1>`)
})

routeApi.get('/infos',(req:express.Request,res:express.Response)=>{
    res.status(200).send(`welcome to my infos`);
})

export default routeApi