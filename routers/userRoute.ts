import express from 'express';
import bcrypt from 'bcrypt';
import {body, validationResult} from 'express-validator';
const routeUser:express.Router=express.Router();

routeUser.get('/',(req:express.Request,res:express.Response)=>{
    res.status(200).send(`welcome too my user Api`)
})

routeUser.post('/login',(req:express.Request,res:express.Response)=>{
    res.status(200).json({
        message:" data sent",
        data:req.body
    })
})

routeUser.post('/register',[ body('name').not().isEmpty().withMessage("it's empty"),
body('email').isEmail().withMessage("it's not an email address"),
body('password').isLength({min:6}).withMessage("minimu 6 characters")
], async(req:express.Request,res:express.Response)=>{

    try {

        let {  name,email,password }=req.body;

        let salt=  await bcrypt.genSalt(10);
        let hs=  await bcrypt.hash(password,salt);
        const error=validationResult(req)

        if(!error.isEmpty()) {
            return res.status(400).json({error: error.array()})
        }

        res.status(200).json({
            user:{name,email,password},
            passwordHash:hs
    
        })
        
    } catch (error) {

        console.log(error)
    }
   
})

export default routeUser;