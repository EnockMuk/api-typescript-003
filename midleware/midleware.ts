import express from 'express'

// we use midle ware to keep track of every single request between user and server
let midleCheck=(req:express.Request,res:express.Response,next:express.NextFunction)=>{

    let url=req.url;
    let method=req.method;
    let date=new Date().toLocaleDateString();
    let time=new Date().toLocaleTimeString();
    let result:string=`[${url}] - [${method}]-[${time}]-[${time}]`;
    console.log(result);
    
    next();
};
export default midleCheck;