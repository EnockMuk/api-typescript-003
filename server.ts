import express from 'express'
import midleCheck from './midleware/midleware';
import routeApi from './routers/apiRoute'
import routeUser from './routers/userRoute';


const hostname:string='127.0.1.1';
const port:number=5001;

const server:express.Application=express();
server.use(midleCheck)
server.use(express.json())
//server.use(express.urlencoded())
server.use('/api',routeApi)
server.use('/user',routeUser)

server.listen(port,()=>`this ${hostname} is running on ${port}`)